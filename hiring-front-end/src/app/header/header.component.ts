import { Component, OnInit } from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import { StorageService } from '../shared/storage.service'
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [StorageService]
})
export class HeaderComponent implements OnInit {

  constructor(private storageService: StorageService) { }

  userID: string;
  IDuser: string
  userContent: object
  userRepo: object
  userFol: object

  ngOnInit() {
  }

  getUserID(user){
    this.userID = user
    this.storageService.getUser(this.userID).subscribe(
      user => {this.userContent = user;});
    this.storageService.getUserRepos(this.userID).subscribe(
      userRe => {this.userRepo = userRe;});
    this.storageService.getUserFoll(this.userID).subscribe(
      userFo => {this.userFol = userFo;});
  }
  

}
