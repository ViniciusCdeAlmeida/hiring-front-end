import { Component, OnInit, Input} from '@angular/core';
import {MatGridListModule} from '@angular/material/grid-list';

import {MatDividerModule} from '@angular/material/divider';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor() { }

  @Input() userData: any;
  @Input() userRepos: any;
  @Input() userFollws: any;

  regularDate:any

  ngOnInit() {
    console.log(this.userData)
    this.regularDate = new Date(this.userData.created_at).toLocaleDateString();
  }

}
