import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';


@Injectable()
export class StorageService {
  constructor(private http: Http,) { }

  private urlGitApi: string = "https://api.github.com";

  getUser(id) {
    return this.http.get(this.urlGitApi + '/users/' + id)
      .map(res => res.json());
  }
  getUserRepos(id){
    return this.http.get(this.urlGitApi + '/users/' + id + '/repos')
      .map(res => res.json());
  }

  getUserFoll(id){
    return this.http.get(this.urlGitApi + '/users/' + id + '/followers')
      .map(res => res.json());
  }
  
}